﻿using ADO.DAL.Readers;
using ADO.Data.DbModels;
using ADO.Data.Models;
using ADO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ADO.DAL
{
    public class SqlDatabaseAccessService : IDatabaseAccessService
    {
        private readonly SqlCommand _dbCommand;
        private readonly SqlConnection _connection;

        public SqlDatabaseAccessService(IDbCommand dbCommand, IDbConnection connection)
        {
            _dbCommand = dbCommand as SqlCommand;
            _connection = connection as SqlConnection;
        }

        public IEnumerable<OrderDetails> GetOrderDetails(int id)
        {
            try
            {
                _dbCommand.CommandText = "SELECT Orders.OrderID, Products.ProductName" +
                " From Orders join[Order Details] on Orders.OrderID = [Order Details].OrderID" +
                $" join Products on[Order Details].ProductID = Products.ProductID where Orders.OrderID = {id}";

                _connection.Open();
                var reader = _dbCommand.ExecuteReader();
                var result = new OrderDetailsReader().GetOrderDetails(reader);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception($"Exception durring get data from db.Error message: {e.Message} ");
            }
            finally
            {
                _connection.Close();
            }
        }

        public IEnumerable<Order> GetOrders()
        {
            try
            {
                _dbCommand.CommandText = "SELECT * from Orders";
                _connection.Open();
                return GetOrdersFromReader();
            }
            catch (Exception e)
            {
                throw new Exception($"Exception durring get data from db.Error message: {e.Message} ");
            }
            finally
            {
                _connection.Close();
            }
        }

        public void UpdateOrder(DbOrderModel newOrderData)
        {
            try
            {
                _dbCommand.CommandText = $"SELECT * from Orders Where OrderID = {newOrderData.OrderId}";
                _connection.Open();
                var order = GetOrdersFromReader().ElementAt(0);
                if (order.Status == Status.NotSending)
                {
                    _dbCommand.CommandText = $"UPDATE Orders set CustomerID = '{newOrderData.CustomerId}', EmployeeID = '{newOrderData.EmployeeId}', RequiredDate = '{newOrderData.RequiredDate}', " +
                        $"ShipVia = '{newOrderData.ShipVia}', Freight  = '{newOrderData.Freight}', ShipName = '{newOrderData.ShipName}', ShipAddress = '{newOrderData.ShipAddress}', ShipCity = '{newOrderData.ShipCity}', " +
                        $"ShipRegion = '{newOrderData.ShipRegion}', ShipPostalCode = '{newOrderData.ShipPostalCode}', ShipCountry = '{newOrderData.ShipCountry}' WHERE OrderID = {newOrderData.OrderId}";
                    _dbCommand.Parameters.Clear();
                    _dbCommand.ExecuteNonQuery();
                }
                else
                {
                    throw new Exception("Cannot update order with status delivered and not delivered");
                }
            }
            finally
            {
                _connection.Close();
            }
        }

        public void CreateOrder(DbOrderModel order)
        {
            try
            {
                _dbCommand.CommandText = $"INSERT INTO Orders VALUES('{order.CustomerId}','{order.EmployeeId}'," +
                    $"'{order.OrderDate}','{order.RequiredDate}','{order.ShippedDate}','{order.ShipVia}','{order.Freight}'," +
                    $"'{order.ShipName}','{order.ShipAddress}','{order.ShipCity}','{order.ShipRegion}','{order.ShipPostalCode}','{order.ShipCountry}')";

                _connection.Open();
                _dbCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception($"Exception durring get data from db.Error message: {e.Message} ");
            }
            finally
            {
                _connection.Close();
            }
        }

        public void DeleteOrder(int id)
        {
            try
            {
                _dbCommand.CommandText = $"delete from [Order Details] where [Order Details].OrderID = {id};" +
                    $" delete from Orders WHERE OrderID = {id} AND (Orders.OrderDate = null OR Orders.ShippedDate = null)";
                _connection.Open();
                _dbCommand.ExecuteNonQuery();
            }
            finally
            {
                _connection.Close();
            }
        }

        public void SetStatusToNotDelivered(int id)
        {
            try
            {
                _dbCommand.CommandText = $"UPDATE Orders Set OrderDate = '{DateTime.Now}' WHERE OrderID = {id}";
                _connection.Open();
                _dbCommand.ExecuteNonQuery();
            }
            finally
            {
                _connection.Close();
            }
        }

        public void SetStatusToDelivered(int id)
        {
            try
            {
                _connection.Open();
                var transaction = _connection.BeginTransaction();
                _dbCommand.Transaction = transaction;
                _dbCommand.CommandText = $"UPDATE Orders Set ShippedDate = '{DateTime.Now}' WHERE OrderID = {id}";
                _dbCommand.ExecuteNonQuery();
                transaction.Commit();
            }
            finally
            {
                _connection.Close();
            }
        }

        public IEnumerable<OrderHistory> GetOrderHistory(string customer)
        {
            try
            {
                _dbCommand.CommandText = $"exec CustOrderHist {customer}";
                _connection.Open();
                var reader = _dbCommand.ExecuteReader();
                return new OrderHistoryReader().GetOrderHistories(reader);
            }
            finally
            {
                _connection.Close();
            }
        }

        public IEnumerable<OrderDetailHistory> GetOrderDetailHistory(int id)
        {
            try
            {
                _dbCommand.CommandText = $"exec CustOrdersDetail {id}";
                _connection.Open();
                var reader = _dbCommand.ExecuteReader();
                return new OrderDetailsHistoryReader().GetOrderDetailHistories(reader);
            }
            finally
            {
                _connection.Close();
            }
        }

        private IEnumerable<Order> GetOrdersFromReader()
        {
            var reader = _dbCommand.ExecuteReader();
            var result = new OrderReader().GetOrders(reader);
            return result;
        }
    }
}