﻿using ADO.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace ADO.DAL
{
    public class DbMssqlConnectionFactory : IConnectionFactory
    {

        public IDbCommand GetCommand(IDbConnection connection)
        {
            return connection.CreateCommand();
        }

        public IDbConnection GetConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }
    }
}