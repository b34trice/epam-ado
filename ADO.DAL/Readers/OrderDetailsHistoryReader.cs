﻿using ADO.Data.DbModels;
using ADO.Interfaces.Readers;
using System.Collections.Generic;
using System.Data.Common;

namespace ADO.DAL.Readers
{
    public class OrderDetailsHistoryReader : IOrderDetailsHistoryReader
    {
        public List<OrderDetailHistory> GetOrderDetailHistories(DbDataReader reader)
        {
            var result = new List<OrderDetailHistory>();
            while (reader.Read())
            {
                result.Add(
                    new OrderDetailHistory
                    {
                        ProductName = reader.GetString(0),
                        UnitPrice = reader.GetDecimal(1),
                        Quantity = reader.GetInt16(2),
                        Discount = reader.GetInt32(3),
                        ExtendedPrice = reader.GetDecimal(4)
                    }
                    );
            }
            return result;
        }
    }
}