﻿using ADO.Data.DbModels;
using ADO.Interfaces.Readers;
using System.Collections.Generic;
using System.Data.Common;

namespace ADO.DAL.Readers
{
    public class OrderHistoryReader : IOrderHistoryReader
    {
        private const int PRODUCT_NAME_INDEX = 0;
        private const int PRODUCT_TOTAL_INDEX = 1;

        public List<OrderHistory> GetOrderHistories(DbDataReader reader)
        {
            var result = new List<OrderHistory>();
            while (reader.Read())
            {
                result.Add(
                    new OrderHistory
                    {
                        ProductName = reader.GetString(PRODUCT_NAME_INDEX),
                        Total = reader.GetInt32(PRODUCT_TOTAL_INDEX)
                    }
                    );
            }
            return result;
        }
    }
}
