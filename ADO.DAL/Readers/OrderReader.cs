﻿using ADO.Data.Models;
using ADO.Interfaces.Readers;
using System.Collections.Generic;
using System.Data.Common;

namespace ADO.DAL
{
    public class OrderReader : IOrderReader
    {
        private const int ORDER_ID_INDEX = 0;
        private const int CUSTOMER_ID_INDEX = 1;
        private const int EMPLOYEE_ID_INDEX = 2;
        private const int ORDER_DATE_INDEX = 3;
        private const int REQUIRED_DATE_INDEX = 4;
        private const int SHIPPED_DATE_INDEX = 5;
        private const int SHIP_VIA_INDEX = 6;
        private const int FREIGHT_INDEX = 7;
        private const int SHIP_NAME_INDEX = 8;
        private const int SHIP_ADDRESS_INDEX = 9;
        private const int SHIP_CITY_INDEX = 10;
        private const int SHIP_REGION_INDEX = 11;
        private const int SHIP_POSTAL_CODE_INDEX = 12;
        private const int SHIP_COUNTRY_NDEX = 13;

        public List<Order> GetOrders(DbDataReader reader)
        {
            var result = new List<Order>();
            while (reader.Read())
            {
                result.Add(new Order
                {
                    OrderId = reader.GetInt32(ORDER_ID_INDEX),
                    CustomerId = reader.IsDBNull(CUSTOMER_ID_INDEX) ? null : reader.GetString(CUSTOMER_ID_INDEX),
                    EmployeeId = reader.IsDBNull(EMPLOYEE_ID_INDEX) ? default : reader.GetInt32(EMPLOYEE_ID_INDEX),
                    OrderDate = reader.IsDBNull(ORDER_DATE_INDEX) ? default : reader.GetDateTime(ORDER_DATE_INDEX),
                    RequiredDate = reader.IsDBNull(REQUIRED_DATE_INDEX) ? default : reader.GetDateTime(REQUIRED_DATE_INDEX),
                    ShippedDate = reader.IsDBNull(SHIPPED_DATE_INDEX) ? default : reader.GetDateTime(SHIPPED_DATE_INDEX),
                    ShipVia = reader.IsDBNull(SHIP_VIA_INDEX) ? default : reader.GetInt32(SHIP_VIA_INDEX),
                    Freight = reader.IsDBNull(FREIGHT_INDEX) ? default : reader.GetDecimal(FREIGHT_INDEX),
                    ShipName = reader.IsDBNull(SHIP_NAME_INDEX) ? default : reader.GetString(SHIP_NAME_INDEX),
                    ShipAddress = reader.IsDBNull(SHIP_ADDRESS_INDEX) ? default : reader.GetString(SHIP_ADDRESS_INDEX),
                    ShipCity = reader.IsDBNull(SHIP_CITY_INDEX) ? default : reader.GetString(SHIP_CITY_INDEX),
                    ShipRegion = reader.IsDBNull(SHIP_REGION_INDEX) ? default : reader.GetString(SHIP_REGION_INDEX),
                    ShipPostalCode = reader.IsDBNull(SHIP_POSTAL_CODE_INDEX) ? default : reader.GetString(SHIP_POSTAL_CODE_INDEX),
                    ShipCountry = reader.IsDBNull(SHIP_COUNTRY_NDEX) ? default : reader.GetString(SHIP_COUNTRY_NDEX),
                    Status = GetStatus(reader)
                });
            }
            return result;
        }

        private Status GetStatus(DbDataReader reader)
        {
            return reader.IsDBNull(ORDER_DATE_INDEX)
                ? Status.NotSending
                : reader.IsDBNull(SHIPPED_DATE_INDEX) ? Status.NotDelivered : Status.Delivered;
        }
    }
}