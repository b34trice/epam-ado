﻿using ADO.Data.Models;
using ADO.Interfaces.Readers;
using System.Collections.Generic;
using System.Data.Common;

namespace ADO.DAL.Readers
{
    public class OrderDetailsReader : IOrderDetailReader
    {
        private const int ID_INDEX = 0;
        private const int NAME_INDEX = 1;

        public List<OrderDetails> GetOrderDetails(DbDataReader reader)
        {
            var result = new List<OrderDetails>();
            while (reader.Read())
            {
                result.Add(new OrderDetails
                {
                    OrderId = reader.GetInt32(ID_INDEX),
                    ProductName = reader.GetString(NAME_INDEX)
                });
            }
            return result;
        }
    }
}