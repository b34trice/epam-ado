﻿using ADO.Data.DbModels;
using ADO.Data.Models;
using System.Collections.Generic;

namespace ADO.Interfaces
{
    public interface IDatabaseAccessService
    {
        IEnumerable<Order> GetOrders();

        IEnumerable<OrderDetails> GetOrderDetails(int id);

        void UpdateOrder(DbOrderModel newOrderData);

        void CreateOrder(DbOrderModel order);

        void DeleteOrder(int id);

        void SetStatusToNotDelivered(int id);

        void SetStatusToDelivered(int id);

        IEnumerable<OrderHistory> GetOrderHistory(string customer);

        IEnumerable<OrderDetailHistory> GetOrderDetailHistory(int id);
    }
}