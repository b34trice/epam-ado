﻿using System.Data;
using System.Data.Common;

namespace ADO.Interfaces
{
    public interface IConnectionFactory
    {
        IDbCommand GetCommand(IDbConnection connection);
        IDbConnection GetConnection(string connectionString);
    }
}
