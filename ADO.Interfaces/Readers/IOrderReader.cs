﻿using ADO.Data.Models;
using System.Collections.Generic;
using System.Data.Common;

namespace ADO.Interfaces.Readers
{
    public interface IOrderReader
    {
        List<Order> GetOrders(DbDataReader reader);
    }
}
