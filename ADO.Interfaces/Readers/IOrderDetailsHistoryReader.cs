﻿using ADO.Data.DbModels;
using System.Collections.Generic;
using System.Data.Common;

namespace ADO.Interfaces.Readers
{
    public interface IOrderDetailsHistoryReader
    {
        List<OrderDetailHistory> GetOrderDetailHistories (DbDataReader reader);
    }
}
