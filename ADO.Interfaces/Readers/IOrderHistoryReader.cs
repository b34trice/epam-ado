﻿using ADO.Data.DbModels;
using System.Collections.Generic;
using System.Data.Common;

namespace ADO.Interfaces.Readers
{
    public interface IOrderHistoryReader
    {
        List<OrderHistory> GetOrderHistories(DbDataReader reader);
    }
}