﻿using ADO.Data.Models;
using System.Collections.Generic;
using System.Data.Common;

namespace ADO.Interfaces.Readers
{
    public interface IOrderDetailReader
    {
        List<OrderDetails> GetOrderDetails(DbDataReader reader);
    }
}
