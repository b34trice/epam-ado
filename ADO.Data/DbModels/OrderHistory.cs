﻿namespace ADO.Data.DbModels
{
    public class OrderHistory
    {
        public string ProductName { get; set; }

        public int Total { get; set; }
    }
}