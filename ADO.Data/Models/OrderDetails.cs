﻿namespace ADO.Data.Models
{
    public class OrderDetails
    {
        public int OrderId { get; set; }
        public string ProductName { get; set; }
    }
}