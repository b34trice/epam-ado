﻿using ADO.Data.DbModels;

namespace ADO.Data.Models
{
    public class Order : DbOrderModel
    {
        public Status Status { get; set; }
    }
}