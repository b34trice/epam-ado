﻿namespace ADO.Data.Models
{
    public enum Status
    {
        NotSending,
        Delivered,
        NotDelivered
    }
}