﻿using ADO.Data.DbModels;
using ADO.Data.Models;
using ADO.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Unity;
using Xunit;

namespace ADO.DAL.UnitTests
{
    public class DatabaseAccessServiceUnitTests
    {
        private readonly IDatabaseAccessService _service;
        private readonly IUnityContainer _container;

        public DatabaseAccessServiceUnitTests()
        {
            _container = new UnityContainer();
            RegisterDependency();
            _service = _container.Resolve<IDatabaseAccessService>();
        }

        private void RegisterDependency()
        {
            var factory = new DbMssqlConnectionFactory();
            var connection = factory.GetConnection(ConfigurationManager.ConnectionStrings["mssql"].ConnectionString);
            var command = factory.GetCommand(connection) as SqlCommand;
            _container.RegisterInstance(connection);
            _container.RegisterInstance<IDbCommand>(command);
            _container.RegisterType<IDatabaseAccessService, SqlDatabaseAccessService>();
        }

        public class GetOrdersUnitTest : DatabaseAccessServiceUnitTests
        {
            [Fact]
            public void GetOrders_GetOrdersFromDb()
            {
                var actualResult = _service.GetOrders();
            }
        }

        public class GetOrderDetailGetValidOrders : DatabaseAccessServiceUnitTests
        {
            [Theory]
            [InlineData(10500)]

            public void GetOrdersDetails_GetOrdersFromDb(int orderId)
            {
                var expectedResult = new List<OrderDetails>
                {
                    new OrderDetails
                    {
                        OrderId = orderId,
                        ProductName = "Genen Shouyu"
                    },
                    new OrderDetails
                    {
                        OrderId = orderId,
                        ProductName = "Rossle Sauerkraut"
                    }
                };
                var actualResult = new List<OrderDetails>(_service.GetOrderDetails(orderId));
                Assert.Equal(expectedResult.Count, actualResult.Count);
                for (int i = 0; i < expectedResult.Count; i++)
                {
                    Assert.Equal(expectedResult[i].ProductName, actualResult[i].ProductName);
                    Assert.Equal(expectedResult[i].OrderId, actualResult[i].OrderId);
                }
            }
        }

        public class UpdateOrderUnitTests : DatabaseAccessServiceUnitTests
        {
            [Fact]
            public void UpdateOrder_UpdateOrderInDb()
            {
                var order = new DbOrderModel
                {
                    OrderId = 10300,
                    CustomerId = "BLONP",
                    EmployeeId = 2,
                    Freight = 0
                };
                Assert.Throws<Exception>(() => _service.UpdateOrder(order));
            }
        }

        public class CreateOrderUnitTests : DatabaseAccessServiceUnitTests
        {
            [Fact]
            public void CreateOrder_CreatenewOrderInDb()
            {
                var order = new DbOrderModel
                {
                    CustomerId = "BLONP",
                    EmployeeId = 2,
                    Freight = 0,
                    OrderDate = DateTime.Now,
                    RequiredDate = DateTime.Now,
                    ShippedDate = DateTime.Now,
                    ShipVia = 2,
                    ShipRegion = string.Empty,
                    ShipAddress = string.Empty,
                    ShipCity = string.Empty,
                    ShipCountry = string.Empty,
                    ShipName = string.Empty,
                    ShipPostalCode = string.Empty
                };
                _service.CreateOrder(order);
            }
        }

        public class DeleteOrderUnitTests : DatabaseAccessServiceUnitTests
        {
            [Theory]
            [InlineData(10400)]
            public void DeleteOrder_DeleteOrderFromDb(int id)
            {
                _service.DeleteOrder(id);
            }
        }

        public class SetStatusToNotDeliveredUnitTests : DatabaseAccessServiceUnitTests
        {
            [Theory]
            [InlineData(10401)]
            public void SetStatusToNotDelivered_ChangeOrderStatusInDb(int id)
            {
                _service.SetStatusToNotDelivered(id);
            }
        }

        public class SetStatusToDeliveredUnitTests : DatabaseAccessServiceUnitTests
        {
            [Theory]
            [InlineData(10402)]
            public void SetStatusToDelivered_ChangeOrderStatusInDb(int id)
            {
                _service.SetStatusToDelivered(id);
            }
        }

        public class GetOrderHistoryUnitTests : DatabaseAccessServiceUnitTests
        {
            [Theory]
            [InlineData("LAZYK")]
            public void GetOrderHistory_GetHistory(string customer)
            {
                var expectedResult = new List<OrderHistory>
                {
                     new OrderHistory
                     {
                          ProductName = "Boston Crab Meat",
                          Total = 10
                     },new OrderHistory
                     {
                          ProductName = "Queso Cabrales",
                          Total = 10
                     }
                };
                var actualResult = new List<OrderHistory>(_service.GetOrderHistory(customer));

                Assert.Equal(expectedResult.Count, actualResult.Count);
                for (int i = 0; i < expectedResult.Count; i++)
                {
                    Assert.Equal(expectedResult[i].ProductName, actualResult[i].ProductName);
                    Assert.Equal(expectedResult[i].Total, actualResult[i].Total);
                }
            }
        }

        public class GetOrderDetailHistoryUnitTests : DatabaseAccessServiceUnitTests
        {
            [Theory]
            [InlineData(10405)]
            public void GetOrderDetailHistory_GetDetailsHistory(int orderId)
            {
                var expectedResult = new List<OrderDetailHistory>
                {
                    new OrderDetailHistory
                    {
                        ProductName = "Aniseed Syrup",
                        UnitPrice = 8.00M,
                        Quantity = 50,
                        Discount =0,
                        ExtendedPrice = 400.00M
                    }
                };
                var actualResult = new List<OrderDetailHistory>(_service.GetOrderDetailHistory(orderId));

                Assert.Equal(expectedResult.Count, actualResult.Count);
                for (int i = 0; i < expectedResult.Count; i++)
                {
                    Assert.Equal(expectedResult[i].ProductName, actualResult[i].ProductName);
                    Assert.Equal(expectedResult[i].UnitPrice, actualResult[i].UnitPrice);
                    Assert.Equal(expectedResult[i].Quantity, actualResult[i].Quantity);
                    Assert.Equal(expectedResult[i].Discount, actualResult[i].Discount);
                    Assert.Equal(expectedResult[i].ExtendedPrice, actualResult[i].ExtendedPrice);
                }
            }
        }
    }
}