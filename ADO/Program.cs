﻿using ADO.DAL;
using System.Data.SqlClient;

namespace ADO
{
    class Program
    {
        public static void Main(string[] args)
        {
            var factory = new DbMssqlConnectionFactory();
            var connection = factory.GetConnection("data source=localhost;initial catalog=Northwind;integrated security=True;MultipleActiveResultSets=True;") as SqlConnection;
            var command = factory.GetCommand(connection) as SqlCommand;
            var service = new SqlDatabaseAccessService(command, connection);
        }
    }
}